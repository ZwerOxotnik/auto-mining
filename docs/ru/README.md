# Команды

Хотите прочитать на другом языке? | [English](/README.md)
|---|---|

## Быстрые ссылки

[Список изменений](/changelog.txt)
| --- |

## Содержание

* [Введение](#overview)
* [Сообщить об ошибки](#issue)
* [Запросить функцию](#feature)
* [Установка](#installing)
* [Лицензия](#license)

## <a name="overview"></a> Введение

Просто напишите "/auto-mining" или добудьте руду для автоматичесой добычи или нажмите shift+M. Также вы можно строить и т.д. в тоже самое время когда вы добываете руду. Если вы застряли, то напишите ещё раз ту команду или используйте клавиши wasd или нажмите правую кнопку мыши.

## <a name="issue"></a> Нашли ошибку?

Пожалуйста, сообщайте о любых проблемах или ошибках в документации, вы можете помочь нам submitting an issue на нашем GitHub репозитории или сообщите на [mods.factorio.com](https://mods.factorio.com/mod/auto-mining/discussion).

## <a name="feature"></a> Хотите новую функцию?

Вы можете *запросить* новую функцию submitting an issue на нашем GitHub репозитории или сообщите на [mods.factorio.com](https://mods.factorio.com/mod/auto-mining/discussion).

## <a name="installing"></a> Установка

Если вы скачали zip архив:

* просто поместите его в директорию модов.

Для большей информации, смотрите [вики Factorio "загрузка и установка модов"](https://wiki.factorio.com/Modding/ru#.D0.97.D0.B0.D0.B3.D1.80.D1.83.D0.B7.D0.BA.D0.B0_.D0.B8_.D1.83.D1.81.D1.82.D0.B0.D0.BD.D0.BE.D0.B2.D0.BA.D0.B0_.D0.BC.D0.BE.D0.B4.D0.BE.D0.B2).

## <a name="license"></a> Лицензия

Этот проект защищен авторским правом © 2019-2020 ZwerOxotnik \<zweroxotnik@gmail.com\>.

Использование исходного кода, включенного здесь, регламентируется Apache License, Version 2.0. Смотрите [LICENSE](/LICENSE) файл для разбора.

[homepage]: http://mods.factorio.com/mod/auto-mining
[Factorio]: https://factorio.com/
