data:extend(
{
	{
		type = 'custom-input',
		name = 'stop-auto-mining-1',
		key_sequence = '',
		linked_game_control = 'move-down',
		consuming = 'none',
		enabled = true
	},
	{
		type = 'custom-input',
		name = 'stop-auto-mining-2',
		key_sequence = '',
		linked_game_control = 'move-left',
		consuming = 'none',
		enabled = true
	},
	{
		type = 'custom-input',
		name = 'stop-auto-mining-3',
		key_sequence = '',
		linked_game_control = 'move-right',
		consuming = 'none',
		enabled = true
	},
	{
		type = 'custom-input',
		name = 'stop-auto-mining-4',
		key_sequence = '',
		linked_game_control = 'move-up',
		consuming = 'none',
		enabled = true
	},
	{
		type = 'custom-input',
		name = 'stop-auto-mining-5',
		key_sequence = '',
		linked_game_control = 'mine',
		consuming = 'none',
		enabled = true
	},
	{
		type = 'custom-input',
		name = 'toggle-auto-mining',
		key_sequence = 'SHIFT + M',
		consuming = 'game-only',
		enabled = true
	}
})