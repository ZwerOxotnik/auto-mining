# Auto mining

Read this in another language | [English](/README.md)
|---|---|

## Quick Links

[Changelog](/changelog.txt)
| --- |

## Contents

* [Overview](#overview)
* [Issues](#issue)
* [Features](#feature)
* [Installing](#installing)
* [License](#license)

## Overview

Just write "/auto-mining" or mine a ore for auto mining or press shift+M. And also you can build etc at the same time as you are mining. If you stuck, in that case, just write again the command or WASD buttons or press the right mouse button.

## <a name="issue"></a> Found an Issue?

Please report any issues or a mistake in the documentation, you can help us by submitting an issue to our GitHub Repository or on [mods.factorio.com](https://mods.factorio.com/mod/auto-mining/discussion).

## <a name="feature"></a> Want a Feature?

You can *request* a new feature by submitting an issue to our GitHub Repository or on [mods.factorio.com](https://mods.factorio.com/mod/auto-mining/discussion).

## Installing

If you have downloaded a zip archive:

* simply place it in your mods directory.

For more information, see [Installing Mods on the Factorio wiki](https://wiki.factorio.com/index.php?title=Installing_Mods).

## License

This project is copyright © 2019-2020 ZwerOxotnik \<zweroxotnik@gmail.com\>.

Use of the source code included here is governed by the Apache License, Version 2.0. See the [LICENSE](/LICENSE) file for details.

[homepage]: http://mods.factorio.com/mod/auto-mining
[Factorio]: https://factorio.com/
